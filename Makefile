PHP_CLI=docker-compose run --rm app-php-cli

tree-building:
	$(PHP_CLI) php yii referral-tree --clientUid=$(clientUid)

count-tree-level:
	$(PHP_CLI) php yii count-tree-level --clientUid=$(clientUid)

profit:
	$(PHP_CLI) php yii profit --clientUid=$(clientUid) --startTime=$(startTime) --endTime=$(endTime)

volume:
	$(PHP_CLI) php yii volume --clientUid=$(clientUid) --startTime=$(startTime) --endTime=$(endTime)

referrals:
	$(PHP_CLI) php yii referrals --clientUid=$(clientUid) --directRefs=$(directRefs)

up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up app-init

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

app-cli:
	$(PHP_CLI) php yii help $(arg)

app-init: app-composer-install

app-composer-install:
	$(PHP_CLI) composer install
