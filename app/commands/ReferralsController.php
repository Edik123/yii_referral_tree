<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

class ReferralsController extends Controller
{
    /**
     * @var int
     */
    protected $clientUid;

    /**
     * @var string
     */
    protected $directRefs;

    public function options($actionID)
    {
        return ['clientUid', 'directRefs'];
    }

    /**
     * Сount the number of direct referrals and the number of all client referrals
     *
     * @return int Exit code
     */
    public function actionIndex()
    {
        $count = 0;
        $count = ($this->directRefs == "true") ?
            count(User::findAll(['partner_id' => $this->clientUid])):
            $count = $this->countRefs($this->clientUid, $count);

        if ($this->directRefs == "true") {
            $this->stdout("DirectRefs: ".$count."\n");
        } else {
            $this->stdout("RefsCount: ".$count."\n");
        }

        return ExitCode::OK;
    }

    /**
     * @param int $clientUid
     * @param int $count
     * @return int
     */
    private function countRefs($clientUid, &$count): int
    {
        $users = User::findAll(['partner_id' => $clientUid]);
        foreach ($users as $user) {
            if ($user->client_uid === $user->partner_id) continue;

            $count++;
            $this->countRefs($user->client_uid, $count);
        }
        return $count;
    }
}