<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

class ReferralTreeController extends Controller
{
    /**
     * @var int
     */
    protected $clientUid;

    public function options($actionID)
    {
        return ['clientUid'];
    }

    /**
     * TODO: Попробовать переписать обход через WITH RECURSIVE MySQL, в теории должно быть быстрее
     * Building a referral tree
     *
     * @return int Exit code
     */
    public function actionIndex()
    {
        $this->stdout("|--".$this->clientUid."\n");
        $this->printReferralTree($this->clientUid);

        return ExitCode::OK;
    }

    /**
     * @param int $clientUid
     * @param int $level
     */
    private function printReferralTree($clientUid, $level = 1)
    {
        $users = User::findAll(['partner_id' => $clientUid]);
        foreach ($users as $user) {
            if ($user->client_uid === $user->partner_id) continue;

            $gaps = str_repeat(' ', $level*5);
            $this->stdout("|".$gaps."|--".$user->client_uid."\n");

            $this->printReferralTree($user->client_uid, $level + 1);
        }
    }
}