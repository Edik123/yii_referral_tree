<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

class CountTreeLevelController extends Controller
{
    /**
     * @var int
     */
    protected $clientUid;
    /**
     * @var int
     */
    private $treeHeight = 0;

    public function options($actionID)
    {
        return ['clientUid'];
    }

    /**
     * Counting the number of levels of the referral grid
     *
     * @return int Exit code
     */
    public function actionIndex()
    {
        $this->treeLevel($this->clientUid);
        $this->stdout("Referral Tree Level: ".$this->getTreeLevel()."\n");

        return ExitCode::OK;
    }

    /**
     * @param int $clientUid
     * @param int $level
     */
    private function treeLevel($clientUid, $level = 0)
    {
        $this->setTreeLevel($level);
        $users = User::findAll(['partner_id' => $clientUid]);
        foreach ($users as $user) {
            if ($user->client_uid === $user->partner_id) continue;

            $this->treeLevel($user->client_uid, $level + 1);
        }
    }

    public function getTreeLevel(): int
    {
        return $this->treeHeight;
    }

    /**
     * @param int $height
     */
    private function setTreeLevel($height)
    {
        if ($height > $this->treeHeight) {
            $this->treeHeight = $height;
        }
    }
}