<?php

namespace app\commands;

use app\models\Trade;
use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

class ProfitController extends Controller
{
    /**
     * @var int
     */
    protected $clientUid;
    /**
     * @var string
     */
    protected $startTime;
    /**
     * @var string
     */
    protected $endTime;

    public function options($actionID)
    {
        return ['clientUid', 'startTime', 'endTime'];
    }

    /**
     * Calculate profitability for a certain period of time
     *
     * @return int Exit code
     */
    public function actionIndex()
    {
        $profitSum = 0;
        $profitSum = $this->profit($this->clientUid, $profitSum);

        $this->stdout("Profit: ".$profitSum." in period: ".$this->startTime." on ".$this->endTime."\n");

        return ExitCode::OK;
    }

    /**
     * @param int $clientUid
     * @param float $sum
     * @return float
     */
    private function profit($clientUid, $sum): float
    {
        $sql = 'SELECT SUM(profit) from trades t where t.login =:login and t.open_time >= :startTime and t.close_time <= :endTime';
        $users = User::findAll(['partner_id' => $clientUid]);
        foreach ($users as $user) {
            if ($user->client_uid === $user->partner_id) continue;
            foreach ($user->accounts as $account) {
                $nodeSum = Trade::findBySql(
                    $sql,
                    [':login' => $account->login, ':startTime' => $this->startTime, ':endTime' => $this->endTime]
                )->scalar();

                $sum += $nodeSum;
            }
            $sum = $this->profit($user->client_uid, $sum);
        }
        return $sum;
    }
}